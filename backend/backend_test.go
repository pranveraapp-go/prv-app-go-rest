package backend_test

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"prv-app-go-rest/backend"
	"testing"
)

const tablesCreationQuery = `CREATE  TABLE IF NOT EXISTS products
(
	id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	productCode VARCHAR(25) NOT NULL,
    name VARCHAR(256) NOT NULL,
	inventory INT NOT NULL,
	price INTEGER NOT NULL,
	status VARCHAR(64) NOT NULL
);

CREATE  TABLE IF NOT EXISTS orders
(
	id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	customerName VARCHAR(50) NOT NULL,
	total INTEGER NOT NULL,
	status VARCHAR(25) NOT NULL
);

CREATE  TABLE IF NOT EXISTS order_items
(
	order_id INTEGER NOT NULL,
	product_id INTEGER NOT NULL,
	quantity INTEGER NOT NULL,
	FOREIGN KEY (order_id) REFERENCES orders(id),
	FOREIGN KEY (product_id) REFERENCES products(id),
	PRIMARY KEY(order_id, product_id)
);

`

var a backend.App

func TestMain(m *testing.M) {
	a = backend.App{}
	a.Initialize()

	ensureTableExsists()
	code := m.Run()

	clearTables()
	os.Exit(code)
}

func clearTables() {
	a.DB.Exec("DELETE FROM products")
	a.DB.Exec("DELETE FROM sqlite_sequence WHERE name = 'products'")
	a.DB.Exec("DELETE FROM orders")
	a.DB.Exec("DELETE FROM sqlite_sequence WHERE name = 'orders'")
	a.DB.Exec("DELETE FROM order_items")
	a.DB.Exec("DELETE FROM sqlite_sequence WHERE name = 'order_items'")
}

func ensureTableExsists() {
	if _, err := a.DB.Exec(tablesCreationQuery); err != nil {
		log.Fatal(err)
	}
}

func TestGetNonExsistentProduct(t *testing.T) {
	clearTables()
	req, _ := http.NewRequest("GET", "/product/11", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusInternalServerError, response.Code)

	var m map[string]string
	json.Unmarshal(response.Body.Bytes(), &m)
	if m["error"] != "sql: no rows in result set" {
		t.Errorf("Expected the 'error' key of the response to be set to 'sql: no rows in result set'"+
			" Got '%s'", m["error"])
	}
}

func TestCreateProduct(t *testing.T) {
	clearTables()
	payload := []byte(`{
	"productCode": "TEST12345",
	"name": "ProductTest",
	"inventory": 1,
	"price": 1,
	"status": "test"}`)
	req, _ := http.NewRequest("POST", "/products", bytes.NewBuffer(payload))
	resp := executeRequest(req)
	checkResponseCode(t, http.StatusOK, resp.Code)

	var m map[string]interface{}
	json.Unmarshal(resp.Body.Bytes(), &m)

	if m["productCode"] != "TEST12345" {
		t.Errorf("Expected productCode to be 'TEST12345'. Got '%v'", m["productCode"])
	}
	if m["name"] != "ProductTest" {
		t.Errorf("Expected name to be 'ProductTest'. Got '%v'", m["name"])
	}
	if m["inventory"] != 1.0 {
		t.Errorf("Expected inventory to be '1'. Got '%v'", m["inventory"])
	}
	if m["price"] != 1.0 {
		t.Errorf("Expected price to be '1'. Got '%v'", m["price"])
	}
	if m["status"] != "test" {
		t.Errorf("Expected status to be 'test'. Got '%v'", m["status"])
	}
	if m["id"] != 1.0 {
		t.Errorf("Expected id to be '1'. Got '%v'", m["id"])
	}

}

func TestGetOrder(t *testing.T) {
	clearTables()
	persistOrder()
	req, _ := http.NewRequest("GET", "/order/1", nil)
	resp := executeRequest(req)
	checkResponseCode(t, http.StatusOK, resp.Code)

	var m map[string]interface{}
	json.Unmarshal(resp.Body.Bytes(), &m)

	if m["customerName"] != "DaisyCuck" {
		t.Errorf("Expected CustomerName to be 'DaisyCuck'. Got '%v'", m["customerName"])
	}
	if m["total"] != 30.0 {
		t.Errorf("Expected name to be 30 . Got '%v'", m["total"])
	}
	if m["status"] != "Shipped" {
		t.Errorf("Expected inventory to be 'Shipped'. Got '%v'", m["status"])
	}
	if m["id"] != 1.0 {
		t.Errorf("Expected id to be '1'. Got '%v'", m["id"])
	}
}

func persistOrder() {
	_, err := a.DB.Exec("INSERT INTO  orders(customerName, total, status)"+
		" VALUES(?, ?, ?)", "DaisyCuck", 30, "Shipped")
	if err != nil {
		log.Fatal(err)
	}
	return
}

func TestCreateOrderItems(t *testing.T) {
	clearTables()
	persistProduct()
	persistOrder()

	payload := []byte(`[{
	"order_id":1,
	"product_id":1,
	"quantity":10
	}]`)
	req, _ := http.NewRequest("POST", "/orderitems", bytes.NewBuffer(payload))
	resp := executeRequest(req)
	checkResponseCode(t, http.StatusOK, resp.Code)

	var sl []map[string]interface{}
	json.Unmarshal(resp.Body.Bytes(), &sl)
	if sl[0]["order_id"] != 1.0 {
		t.Errorf("Expected order_id to be '1'. Got '%v'", sl[0]["order_id"])
	}
	if sl[0]["product_id"] != 1.0 {
		t.Errorf("Expected product_id to be '1' . Got '%v'", sl[0]["product_id"])
	}
	if sl[0]["quantity"] != 10.0 {
		t.Errorf("Expected quantity to be '10'. Got '%v'", sl[0]["quantity"])
	}
}

func TestCreateOrder(t *testing.T) {
	clearTables()
	payload := []byte(`{
	"customerName": "DaisyCuck",
	"total": 30,
	"status": "Shipped",
	"items": []}`)
	req, _ := http.NewRequest("POST", "/orders", bytes.NewBuffer(payload))
	resp := executeRequest(req)
	checkResponseCode(t, http.StatusOK, resp.Code)

	var m map[string]interface{}
	json.Unmarshal(resp.Body.Bytes(), &m)

	if m["customerName"] != "DaisyCuck" {
		t.Errorf("Expected CustomerName to be 'DaisyCuck'. Got '%v'", m["customerName"])
	}
	if m["total"] != 30.0 {
		t.Errorf("Expected name to be 30 . Got '%v'", m["total"])
	}
	if m["status"] != "Shipped" {
		t.Errorf("Expected inventory to be 'Shipped'. Got '%v'", m["status"])
	}
	if m["id"] != 1.0 {
		t.Errorf("Expected id to be '1'. Got '%v'", m["id"])
	}

}

func TestGetProduct(t *testing.T) {
	clearTables()
	persistProduct()

	req, _ := http.NewRequest("GET", "/product/1", nil)
	resp := executeRequest(req)

	checkResponseCode(t, resp.Code, http.StatusOK)

	var m map[string]interface{}

	json.Unmarshal(resp.Body.Bytes(), &m)
	if m["productCode"] != "TEST123" {
		t.Errorf("Expected productCode to be 'TEST123'. Got '%v'", m["productCode"])
	}
	if m["name"] != "ProductTest" {
		t.Errorf("Expected name to be 'ProductTest'. Got '%v'", m["name"])
	}
	if m["inventory"] != 1.0 {
		t.Errorf("Expected inventory to be '1'. Got '%v'", m["inventory"])
	}
	if m["price"] != 1.0 {
		t.Errorf("Expected price to be '1'. Got '%v'", m["price"])
	}
	if m["status"] != "test" {
		t.Errorf("Expected status to be 'test'. Got '%v'", m["status"])
	}
	if m["id"] != 1.0 {
		t.Errorf("Expected id to be '1'. Got '%v'", m["id"])
	}
}

func persistProduct() {
	_, err := a.DB.Exec("INSERT INTO  products(productCode, name, inventory, price, status)"+
		" VALUES(?, ?, ?, ?, ?)", "TEST123", "ProductTest", 1, 1, "test")
	if err != nil {
		log.Fatal(err)
	}
	return
}

func TestGetNonExsistentOrder(t *testing.T) {
	clearTables()
	req, _ := http.NewRequest("GET", "/order/11", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusInternalServerError, response.Code)

	var m map[string]string
	json.Unmarshal(response.Body.Bytes(), &m)
	if m["error"] != "sql: no rows in result set" {
		t.Errorf("Expected the 'error' key of the response to be set to 'sql: no rows in result set'"+
			" Got '%s'", m["error"])
	}
}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	a.Router.ServeHTTP(rr, req)
	return rr
}
