package main

import "prv-app-go-rest/backend"

func main() {
	a := backend.App{}
	a.Port = ":9003"
	a.Initialize()
	a.Run()
}
